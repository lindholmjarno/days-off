import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class WorkDays {

    int days;
    int workers;
    int daysOffPerWorker;
    char[][] chart;
    int[] sum;
    int[] difference;
    boolean found = true;
    char dayOffChar = '#';
    char workDayChar = '.';
    int limit;

    public WorkDays(int days, int workers, int daysOffPerWorker) {
        this.days = days;
        this.workers = workers;
        this.daysOffPerWorker = daysOffPerWorker;
        chart = new char[workers][days];
        sum = difference = new int[days];
    }

    public void run(){
      while(found) {
          limit = 0;
          populateWorkingDays();
          addRandomDayOff();
          printWorkDays();
          calculateSum();
          calculateDifference();
          printChar(days);
          printSumArray();
          printChar(days);
          while (found && limit < 1000 ){
              //printSumArray();
              //printDifference();
              switchDays();
              resetArrays();
              calculateSum();
              calculateDifference();
              ////printChar(days);
              limit++;
          }
          if (!found) {
              printWorkDays();
              printChar(days);
              printSumArray();
          }
      }
    }

    // Just for nicer output...
    public void printChar(int d){
        for (int i = 0; i < d; i++) {
            System.out.print("==");
        }
        System.out.println();
    }

    // Each loop needs empty arrays
    public void resetArrays() {
        sum = new int[days];
        difference = new int[days];
    }

    // Fill matrix with chars which represents workday
    public void populateWorkingDays() {
        for (int i = 0; i < chart.length; i++) {
            for (int j = 0; j < chart[i].length; j++) {
                chart[i][j] = workDayChar;
            }
        }
    }

    // Each worker chart[worker][] gets randomly n chars which represents holiday
    // If current days is holiday generate new random index
    public void addRandomDayOff() {
        int random;
        for (int i = 0; i < chart.length; i++) {
            for (int j = 0; j < daysOffPerWorker; j++) {
                random = ThreadLocalRandom.current().nextInt(0,days);
                while(chart[i][random] == dayOffChar){
                    random = ThreadLocalRandom.current().nextInt(0,days);
                }
                chart[i][random] = dayOffChar;
            }
        }
    }

    // Find all day off for each day and store them to array
    public void calculateSum() {
        for (int i = 0; i < chart.length; i++) {
            for (int j = 0; j < chart[i].length; j++) {
                if (chart[i][j] == dayOffChar) {
                    sum[j]++;
                }
            }
        }
    }

    // Print "calendar" to console
    public void printWorkDays() {
        for (int i = 0; i < chart.length; i++) {
            for (int j = 0; j < chart[i].length; j++) {
                System.out.print(chart[i][j] + " ");
            }
            System.out.println();
        }
    }

    // Print array which stores holiday amount for each day
    public void printSumArray() {
        for (int i = 0; i < sum.length; i++) {
            System.out.print(sum[i] + " ");
        }
        System.out.println();
    }

    // Calculates difference between allowed holidays and currently set holidays
    // if weekend -5, week -2. Counter to calculate weekly.
    public void calculateDifference() {
        int counter = 0;
        for (int i = 0; i < difference.length; i++) {
            if( counter != 0 && ( (counter % 5) == 0 || (counter % 6) == 0 ) ){
               difference[i] = sum[i] - 5;
            } else {
               difference[i] = sum[i] - 2;
           }
           if( counter == 6 ) {
               counter = 0;
            } else {
               counter++;
           }
        }
    }

    // Print array which stores difference between allowed holidays and current ones
    public void printDifference() {
        for (int i = 0; i < difference.length; i++) {
            System.out.print(difference[i] + " ");
        }
        System.out.println();
    }

    // Finds biggest value from difference array and return its index
    public int findBiggest() {
        int maxIndex = 0;
        int maxValue = 0;
        for (int i = 0; i < difference.length; i++) {
            if (difference[i] > maxValue) {
                maxValue = difference[i];
                maxIndex = i;
            }
        }
        return maxIndex;
    }

    // if biggest value in difference array is zero then algorithm is complete and program will stop
    // pick random index and switch it to work day if its possible. If not continue
    // when done update smallest value
    // break
    public void switchDays() {
        if(difference[findBiggest()] == 0) {
            found = false;
            return;
        }else {
            for (int i = 0; i < chart.length; i++) {
                int rand = ThreadLocalRandom.current().nextInt(0, chart.length);
                if (chart[rand][findBiggest()] == dayOffChar) {
                    chart[rand][findBiggest()] = workDayChar;
                    updateSmallest(rand);
                    break;
                }
            }
        }
    }

    // Parameter is randomly picked index.
    // clone array and sort it
    // loop through array, find smallest possible index to switch value
    // When found return
    public void updateSmallest(int index){
        int[] temp = difference.clone();
        Arrays.sort(temp);
        for (int i = 0; i < temp.length; i++) {
            for (int j = 0; j < difference.length; j++) {
                if(difference[j] == temp[i]) {
                    if(chart[index][j] == workDayChar) {
                        chart[index][j] = dayOffChar;
                        return;
                    }
                }
            }
        }
    }
}
